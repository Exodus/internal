/** 
 * @file exofloaterraidadvisor.cpp
 * @brief The raid advisor, watches avatar count of regions.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exofloaterraidadvisor.h"

#include "evonmanager.h"
#include "message.h"

#include "llassetstorage.h"
#include "llassetuploadresponders.h"
#include "llagent.h"
#include "llbutton.h"
#include "llfloaterworldmap.h"
#include "llfile.h"
#include "llfilepicker.h"
#include "llfloaterreg.h"
#include "llinventoryfunctions.h"
#include "llinventorymodel.h"
#include "llnotecard.h"
#include "llnotificationsutil.h"
#include "llscrolllistctrl.h"
#include "llsdserialize.h"
#include "lltextbox.h"
#include "lluictrlfactory.h"
#include "llvfile.h"
#include "llvfs.h"
#include "llviewercontrol.h"
#include "llviewerinventory.h"
#include "llworldmap.h"
#include "llextendedstatus.h"

#define EVON_TYPE "RA"
#define EVON_EXTENTION ".ra.evon"
#define EVON_VERSION "1.1"
#define EVON_VERSION_NUM 1.1

#define PRESET_FOLDER "#Raid Advisor Backups"

// exo_floater_raid_advisor.xml

exoFloaterRaidAdvisor::exoFloaterRaidAdvisor(const LLSD& key) :
	LLFloater(key),
	LLEventTimer(10.f)
{
	mEventTimer.stop();
}

exoFloaterRaidAdvisor::~exoFloaterRaidAdvisor()
{
}

BOOL exoFloaterRaidAdvisor::postBuild()
{
	textTitle = getChild<LLTextBox>("Title");
	textSubTitle = getChild<LLTextBox>("Sub Title");

	simulatorList = getChild<LLScrollListCtrl>("List");
	if (simulatorList)
	{
		simulatorList->setCommitOnSelectionChange(TRUE);
		simulatorList->setCommitCallback(boost::bind(&exoFloaterRaidAdvisor::selectionChange, this));
		simulatorList->setDoubleClickCallback(boost::bind(&exoFloaterRaidAdvisor::onOpenMap, this));
		simulatorList->sortByColumn(gSavedSettings.getString("ExodusRaidAdvisorSortColumn"), gSavedSettings.getBOOL("ExodusRaidAdvisorSortAscending"));
		simulatorList->setSortChangedCallback(boost::bind(&exoFloaterRaidAdvisor::onChangedSort, this));

		if (getData() || getDataOld())
		{
			updateData();
			simulatorList->selectFirstItem();
		}
		else if (textSubTitle)
		{
			textSubTitle->setValue("Add a region by using the World Map!");
		}
	}

	mapPreview = getChild<LLButton>("Map Preview");
	if (mapPreview)
	{
		LLUIImagePtr image = LLUI::getUIImageByID(LLUUID("3a7f1859-ae21-46f6-eeed-9d0e021a0057"));
		
		mapPreview->setImageSelected(image);
		mapPreview->setImageUnselected(image);
		mapPreview->setImagePressed(image);
		mapPreview->setClickedCallback(boost::bind(&exoFloaterRaidAdvisor::onOpenMap, this));
	}

	LLButton* removeItem = getChild<LLButton>("Remove");
	if (removeItem) removeItem->setClickedCallback(boost::bind(&exoFloaterRaidAdvisor::removeItem, this));
	
	LLButton* exportButton = getChild<LLButton>("Export");
	if (exportButton) exportButton->setClickedCallback(boost::bind(&exoFloaterRaidAdvisor::exportData, this));
	
	LLButton* importButton = getChild<LLButton>("Import");
	if (importButton) importButton->setClickedCallback(boost::bind(&exoFloaterRaidAdvisor::importData, this));

	mEventTimer.start();

	return TRUE;
}

BOOL exoFloaterRaidAdvisor::tick()
{
	updateData();

	return FALSE;
}

void exoFloaterRaidAdvisor::onChangedSort()
{
	gSavedSettings.setString("ExodusRaidAdvisorSortColumn", simulatorList->getSortColumnName());
	gSavedSettings.setBOOL("ExodusRaidAdvisorSortAscending", simulatorList->getSortAscending());
}

bool exoFloaterRaidAdvisor::getData(std::string filename, bool local)
{
	mapList.clear();

	if (local) filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", filename);
	
	LLSD data;
	llifstream evon_file(filename);
	if (evon_file.is_open() && evonManager::fromEVON(data, evon_file, EVON_TYPE, EVON_VERSION_NUM) > 0)
	{
		evon_file.close();
		return importData(data);
	}

	return false;
}

bool exoFloaterRaidAdvisor::getDataOld(std::string filename, bool local)
{
	mapList.clear();

	if (local) filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, filename);
	
	LLSD data;
	llifstream xml_file(filename);
	if (xml_file.is_open() && LLSDSerialize::fromXML(data, xml_file) >= 1)
	{
		xml_file.close();
		return importData(data);
	}

	return false;
}

bool exoFloaterRaidAdvisor::importData(LLSD data)
{
	if (data.has("valid"))
	{
		if (data.size() > 100)
		{
			llinfos << "Too many regions listed!" << llendl;

			// Todo: Should probably do a notification here.
		}
		else
		{
			if (data.has("valid")) data.erase("valid");

			mapList = data;
			updateData();

			if (!simulatorList->getSelectedValue()) simulatorList->selectFirstItem();
			return true;
		}
	}

	return false;
}

void exoFloaterRaidAdvisor::importNotecard(const LLInventoryItem* item)
{
	if (item)
	{
		loadNotecardName = item->getName();

		LLSD args, payload;
		args["NAME"] = loadNotecardName;
		payload["owner-key"] = item->getPermissions().getOwner();
		payload["item-key"] = item->getUUID();
		payload["asset-key"] = item->getAssetUUID();
		payload["type"] = (S32)item->getType();

		LLNotificationsUtil::add("ExodusRaidAdvisorImport", args, payload, boost::bind(importNotecardCallback, _1, _2));
	}
}

void exoFloaterRaidAdvisor::importNotecardCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		exoFloaterRaidAdvisor* self =
			LLFloaterReg::getTypedInstance<exoFloaterRaidAdvisor>("exo_raid_advisor");
		if (self)
		{
			if (!self->loadNotecardName.empty())
			{
				LLUUID key = notification["payload"]["asset-key"].asUUID();
				LLUUID* id = new LLUUID(key);
				const LLHost source = LLHost::invalid;

				gAssetStorage->getInvItemAsset(
					source, gAgent.getID(), gAgent.getSessionID(),
					notification["payload"]["owner-key"].asUUID(), LLUUID::null,
					notification["payload"]["item-key"].asUUID(), key,
					(LLAssetType::EType)notification["payload"]["type"].asInteger(),
					&self->loadNotecard, (void*)id, TRUE
				);
			}
		}
	}
}

void exoFloaterRaidAdvisor::loadNotecard(LLVFS* vfs, const LLUUID& id, LLAssetType::EType type, void* userdata, S32 status, LLExtStat ext)
{
	exoFloaterRaidAdvisor* self =
		LLFloaterReg::getTypedInstance<exoFloaterRaidAdvisor>("exo_raid_advisor");
	if (self)
	{
		if (status == LL_ERR_NOERR && !self->loadNotecardName.empty())
		{
			S32 size = vfs->getSize(id, type);
			char* buffer = new char[size];
			vfs->getData(id, type, (U8*)buffer, 0, size);
			if (buffer)
			{
				std::string text(buffer);
				delete buffer;
				std::size_t start = text.find_first_of('#');
				if (start != std::string::npos && start != 0)
				{
					LLSD data;
					std::stringstream notecard(text.substr(start, text.length()));
					std::string versionData, notecardData;
					std::getline(notecard, versionData);
					std::getline(notecard, notecardData);
					std::stringstream final(versionData + "\n" + notecardData);
					if (evonManager::fromEVON(data, final, EVON_TYPE, EVON_VERSION_NUM) > 0)
					{
						self->importData(data);
						self->saveData();
						return;
					}
				}

				llwarns << "Unable to phrase \"" << self->loadNotecardName << "\", aborting import." << llendl;
				self->loadNotecardName.erase();
				LLNotificationsUtil::add("ExodusRaidAdvsorFailed", LLSD());
				return;
			}
		}

		llwarns << "Download failed for \"" << self->loadNotecardName << "\", aborting import." << llendl;
		self->loadNotecardName.erase();
	}
	else
	{
		llwarns << "Unable to find preset manager, cannot import notecard." << llendl;
	}

	LLNotificationsUtil::add("ExodusRaidAdvsorFailed", LLSD());
}

bool exoFloaterRaidAdvisor::saveData(std::string filename, bool local)
{
	if (mapList.size() == 0) return false;

	if (local) filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", filename);

	llofstream evon_file(filename);
	if (evon_file.is_open())
	{
		LLSD data = mapList;
		data["valid"] = true;
		evonManager::toEVON(data, evon_file, EVON_TYPE, EVON_VERSION);
		evon_file.close();

		return true;
	}

	return false;
}

void exoFloaterRaidAdvisor::updateData()
{
	if (mapList.size() == 0 || !getVisible()) return;

	std::string selectedLabel = simulatorList->getSelectedItemLabel(2);
	S32 scrollPosition = simulatorList->getScrollPos();
	simulatorList->clearRows();

	std::string current_sim = gAgent.getRegion()->getName();

	for(LLSD::map_iterator iter = mapList.beginMap();
		iter != mapList.endMap(); ++iter)
	{
		LLSD element = (*iter).second;
		if (element.isMap() && element.has("columns")) // Assume the rest is correct.
		{
			std::string sim_name = element["columns"][2]["value"].asString();
			
			LLSimInfo* info = LLWorldMap::getInstance()->simInfoFromName(sim_name);
			if (info)
			{
				info->updateAgentCount(LLTimer::getElapsedSeconds());
				S32 count = info->getAgentCount();
				
				// Todo: Add general/mature/adult icons here.

				element["columns"][0]["column"]             = "icon";
				element["columns"][0]["type"]               = "text";
				element["columns"][0]["value"]              = " ";

				element["columns"][3]["column"]             = "count";
				element["columns"][3]["type"]               = "text";
				
				if (!info->isDown() && count)
				{
					if (sim_name == current_sim) ++count;

					element["columns"][3]["value"]          = llformat("%d", count);
				}
				else
				{
					if (!info->mFirstAgentRequest)
					{
						if (sim_name == current_sim)
						{
							element["columns"][3]["value"]  = "1";
						}
						else element["columns"][3]["value"] = "0";
					}
					else element["columns"][3]["value"]     = "...";
				}

				LLVector3d pos_global = info->getGlobalOrigin(); U32 grid_x, grid_y;
				LLWorldMipmap::globalToMipmap(pos_global[VX], pos_global[VY], 1, &grid_x, &grid_y);
				LLPointer<LLViewerFetchedTexture> sim_image = LLWorldMap::getInstance()->getObjectsTile(grid_x, grid_y, 1);
				if (sim_image)
				{
					element["columns"][4]["column"]         = "texture";
					element["columns"][4]["type"]           = "text";
					element["columns"][4]["value"]          = sim_image->getID().asString();
				}

				LLScrollListItem* item = simulatorList->addElement(element);
				if (item && info->isDown())
				{
					((LLScrollListText*)item->getColumn(1))->setColor(LLColor4::red);
					((LLScrollListText*)item->getColumn(2))->setColor(LLColor4::red);
					((LLScrollListText*)item->getColumn(3))->setColor(LLColor4::red);
				}
			}
			else
			{
				element["columns"][0]["column"]             = "icon";
				element["columns"][0]["type"]               = "text";
				element["columns"][0]["value"]              = " ";

				element["columns"][3]["column"]             = "count";
				element["columns"][3]["type"]               = "text";
				element["columns"][3]["value"]              = " ";

				element["columns"][4]["column"]             = "texture";
				element["columns"][4]["type"]               = "text";
				element["columns"][4]["value"]              = "";

				LLScrollListItem* item = simulatorList->addElement(element);
				if (item)
				{
					((LLScrollListText*)item->getColumn(1))->setColor(LLColor4::grey);
					((LLScrollListText*)item->getColumn(2))->setColor(LLColor4::grey);
					((LLScrollListText*)item->getColumn(3))->setColor(LLColor4::grey);
				}

				LLMessageSystem* msg = gMessageSystem;

				msg->newMessageFast(_PREHASH_MapNameRequest);
				msg->nextBlockFast(_PREHASH_AgentData);
				msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
				msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
				msg->addU32Fast(_PREHASH_Flags, 2);
				msg->addU32Fast(_PREHASH_EstateID, 0); // Filled in on simulator.
				msg->addBOOLFast(_PREHASH_Godlike, FALSE); // Filled in on simulator.
				msg->nextBlockFast(_PREHASH_NameData);
				msg->addStringFast(_PREHASH_Name, sim_name);

				gAgent.sendReliableMessage();
			}
		}
		else mapList.erase((*iter).first);
	}

	if (mapList.size())
	{
		if (!selectedLabel.empty()) simulatorList->selectItemByLabel(selectedLabel, 1, 2);
		else simulatorList->selectFirstItem();

		simulatorList->setScrollPos(scrollPosition);

		getChild<LLButton>("Export")->setEnabled(true);
	}
	else
	{
		getChild<LLButton>("Export")->setEnabled(false);
	}
}

// static
void exoFloaterRaidAdvisor::addItemCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		exoFloaterRaidAdvisor* self =
			LLFloaterReg::getTypedInstance<exoFloaterRaidAdvisor>("exo_raid_advisor");
		if (self)
		{
			// Todo: Something about duplicates?

			std::string sim_name = response["sim_name"].asString();
			std::string sim_region = notification["payload"]["sim_region"].asString();
			LLStringUtil::trim(sim_name);
			LLStringUtil::trim(sim_region);
			if (!sim_name.empty() && !sim_region.empty())
			{
				LLSD mapItem;
				mapItem["columns"][1]["column"] = "name";
				mapItem["columns"][1]["type"]   = "text";
				mapItem["columns"][1]["value"]  = sim_name;
				mapItem["columns"][2]["column"] = "sim";
				mapItem["columns"][2]["type"]   = "text";
				mapItem["columns"][2]["value"]  = sim_region;

				self->mapList.insert(sim_name, mapItem);
				self->updateData();
				self->saveData();
			}
			else
			{
				// Display notification here?
			}
		}
	}
}

void exoFloaterRaidAdvisor::removeItem()
{
	LLScrollListItem* firstItem = simulatorList->getFirstSelected();
	if (firstItem)
	{
		LLScrollListCell* selectedName = firstItem->getColumn(1);
		if (selectedName)
		{
			std::string sim_name = selectedName->getValue().asString();
			if (!sim_name.empty())
			{
				LLStringUtil::trim(sim_name);

				LLSD args;
				args["NAME"] = sim_name;

				LLSD payload;
				payload["sim_name"] = sim_name;

				LLNotificationsUtil::add("ExodusRaidAdvisorRemove", args, payload, boost::bind(removeItemCallback, _1, _2));
			}
		}
	}
}

// static
void exoFloaterRaidAdvisor::removeItemCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		exoFloaterRaidAdvisor* self =
			LLFloaterReg::getTypedInstance<exoFloaterRaidAdvisor>("exo_raid_advisor");
		if (self)
		{
			std::string sim_name = notification["payload"]["sim_name"].asString();
			if (!sim_name.empty() && self->mapList.has(sim_name)) self->mapList.erase(sim_name);

			self->updateData();
			self->saveData();
			self->simulatorList->selectFirstItem();
		}
	}
}

void exoFloaterRaidAdvisor::onOpenMap()
{
	LLScrollListItem* firstItem = simulatorList->getFirstSelected();
	if (firstItem)
	{
		LLScrollListCell* selectedRegion = firstItem->getColumn(2);
		if (selectedRegion)
		{
			std::string region = selectedRegion->getValue().asString();
			if (!region.empty())
			{
				LLFloaterReg::showInstance("world_map");
				gFloaterWorldMap->trackURL(region, 128, 128, 0);
				gFloaterWorldMap->centerOnTarget(FALSE);
			}
		}
	}
}

void exoFloaterRaidAdvisor::selectionChange()
{
	LLScrollListItem* firstItem = simulatorList->getFirstSelected();
	if (firstItem)
	{
		LLScrollListCell* selectedName = firstItem->getColumn(1);
		LLScrollListCell* selectedRegion = firstItem->getColumn(2);
		if (selectedName && selectedRegion)
		{
			textTitle->setText(selectedName->getValue().asString());
			textSubTitle->setText(selectedRegion->getValue().asString());

			LLScrollListCell* selectedMap = firstItem->getColumn(4);
			if (selectedMap)
			{
				LLUUID map(selectedMap->getValue().asString());

				LLUIImagePtr image = LLUI::getUIImageByID(map.notNull() ? map : LLUUID("3a7f1859-ae21-46f6-eeed-9d0e021a0057"));

				mapPreview->setImageSelected(image);
				mapPreview->setImageUnselected(image);
				mapPreview->setImagePressed(image);

				// Todo: Draw avatar dots over image?
			}
		}
	}
}

void exoFloaterRaidAdvisor::importData()
{
	LLFilePicker& file_picker = LLFilePicker::instance();
	if (file_picker.getOpenFile(LLFilePicker::FFLOAD_EVON_RA))
	{
		getData(file_picker.getFirstFile(), false);
	}
}

void exoFloaterRaidAdvisor::exportData()
{
	LLNotificationsUtil::add("ExodusRaidAdvisorExport", LLSD(), LLSD(), boost::bind(exportDataCallback, _1, _2));
}

void exoFloaterRaidAdvisor::exportDataCallback(const LLSD& notification, const LLSD& response)
{
	S32 selection = LLNotificationsUtil::getSelectedOption(notification, response);
	if (selection < 2)
	{
		exoFloaterRaidAdvisor* self =
			LLFloaterReg::getTypedInstance<exoFloaterRaidAdvisor>("exo_raid_advisor");
		if (self)
		{
			time_t utc_time;
			utc_time = time_corrected();
			LLSD substitution;

			if(selection)
			{
				std::string filename = "[year2,datetime,slt]/[mthnum,datetime,slt]/[day,datetime,slt] [hour24,datetime,slt]:[min,datetime,slt]";
				substitution["datetime"] = (S32)utc_time;
				LLStringUtil::format(filename, substitution);
				filename = "Raid Advisor [" + filename + "]";

				self->exportNotecard(filename);
			}
			else
			{
				std::string filename = "[year2,datetime,slt].[mthnum,datetime,slt].[day,datetime,slt].[hour24,datetime,slt].[min,datetime,slt]";
				substitution["datetime"] = (S32)utc_time;
				LLStringUtil::format(filename, substitution);
				filename = "Raid Advisor (" + filename + ")";

				self->exportFile(filename);
			}
		}
	}
}

void exoFloaterRaidAdvisor::exportFile(std::string filename)
{
	LLFilePicker& file_picker = LLFilePicker::instance();
	if (file_picker.getSaveFile(LLFilePicker::FFSAVE_EVON_RA, filename))
	{
		LLSD data = mapList;
		data["valid"] = true;

		llofstream evon_file(file_picker.getFirstFile());
		if (evon_file.is_open())
		{
			evonManager::toEVON(data, evon_file, EVON_TYPE, EVON_VERSION);
			evon_file.close();
		}
		else
		{
			llwarns << "Unable to export data to:" << llendl;
			llinfos << file_picker.getFirstFile() << llendl;
		}
	}
	else llinfos << "Get save file returned false." << llendl;
}

class exoFloaterRaidAdvisorNotecardCallback :
	public LLInventoryCallback
{
private:
	std::string mNotecardData;
	LLUUID mInventoryFolder;
	
public:
	void set(LLUUID id, std::string data);
	void fire(const LLUUID& id);
};

void exoFloaterRaidAdvisorNotecardCallback::set(LLUUID id, std::string data)
{
	mInventoryFolder = id;
	mNotecardData = data;
}

void exoFloaterRaidAdvisor::exportNotecard(std::string name)
{
	const LLUUID folderID = gInventory.findCategoryByName(EXODUS_FOLDER);
	if (folderID.isNull())
	{
		llinfos << "Could not find \"" << EXODUS_FOLDER << "\" folder in inventory, creating..." << llendl;

		mInventoryFolder.setNull();
		gInventory.createNewCategory(gInventory.getRootFolderID(), LLFolderType::FT_NONE, EXODUS_FOLDER);
	}
	else if (mInventoryFolder.isNull())
	{
		LLInventoryModel::cat_array_t* categoryFolders;
		LLInventoryModel::item_array_t* categoryItems;
		gInventory.getDirectDescendentsOf(folderID, categoryFolders, categoryItems);
		for (int i = 0; i < categoryFolders->count(); ++i)
		{
			const std::string& folderName = categoryFolders->get(i)->getName();
			if (folderName.compare(PRESET_FOLDER) == 0)
			{
				mInventoryFolder = categoryFolders->get(i)->getUUID();
				break;
			}
		}

		if (mInventoryFolder.isNull())
		{
			llinfos << "Could not find \"" << PRESET_FOLDER << "\" in \"" <<
				EXODUS_FOLDER << "\" folder in inventory, creating..." << llendl;

			mInventoryFolder =
				gInventory.createNewCategory(folderID, LLFolderType::FT_NONE, PRESET_FOLDER);
		}

		if (mInventoryFolder.isNull())
		{
			llwarns << "Unable to create \"" << PRESET_FOLDER << "\" folder!" << llendl;
			
			LLSD args;
			args["MESSAGE"] = "Export failed, was unable to create the raid advisor backup folder in your inventory.";
			LLNotificationsUtil::add("SystemMessageTip", args);

			return;
		}
	}

	llinfos << "Exporting notecard to raid advisor backup folder..." << llendl;

	LLSD dataToExport = mapList;
	dataToExport["valid"] = true;

	std::stringstream dataToSave;
	evonManager::toEVON(dataToExport, dataToSave, EVON_TYPE, EVON_VERSION);

	std::string finalNotecard = dataToSave.str();
	if ((finalNotecard.length() * sizeof(std::string::value_type)) <= 65536)
	{
		llinfos << "Attempting to create notecard..." << llendl;

		LLPointer<exoFloaterRaidAdvisorNotecardCallback> callback = new exoFloaterRaidAdvisorNotecardCallback();

		callback->set(mInventoryFolder, finalNotecard);

		create_inventory_item(
			gAgent.getID(), gAgent.getSessionID(),
			mInventoryFolder, LLTransactionID::tnull,
			name + EVON_EXTENTION, "Raid Advisor Backup",
			LLAssetType::AT_NOTECARD, LLInventoryType::IT_NOTECARD,
			NOT_WEARABLE, PERM_ITEM_UNRESTRICTED,
			callback
		);
	}
	else
	{
		llwarns << "Notecard exceeds maximum length! Failed to export notecard." << llendl;
	}
}

void exoFloaterRaidAdvisorNotecardCallback::fire(const LLUUID& id)
{
	if(mNotecardData.empty()) return;
	
	LLInventoryItem *item = gInventory.getItem(id);
	if(item)
	{
		LLNotecard notecardAsset;
		notecardAsset.setText(mNotecardData);

		const LLViewerRegion* region = gAgent.getRegion();
		if (!region)
		{
			llwarns << "Not connected to a region, cannot save notecard." << llendl;
			return;
		}

		std::string agent_url = region->getCapability("UpdateNotecardAgentInventory");
		if (!agent_url.empty())
		{
			llinfos << "Saving notecard via " << agent_url << "." << llendl;
			
			LLAssetID asset_id;
			LLTransactionID tid;
			tid.generate();
			asset_id = tid.makeAssetID(gAgent.getSecureSessionID());

			LLVFile file(gVFS, asset_id, LLAssetType::AT_NOTECARD, LLVFile::APPEND);

			std::ostringstream stringStream;
			notecardAsset.exportStream(stringStream);
			std::string buffer = stringStream.str();
			S32 size = buffer.length() + 1;

			file.setMaxSize(size);
			file.write((U8*)buffer.c_str(), size);

			LLSD body;
			body["item_id"] = id;
			LLHTTPClient::post(agent_url, body, new LLUpdateAgentInventoryResponder(body, asset_id, LLAssetType::AT_NOTECARD));
		}
		else // Todo: Maybe put it into the trash?
		{
			LLSD args;
			args["MESSAGE"] = "Export failed, was unable to save the raid advisor backup.";
			LLNotificationsUtil::add("SystemMessageTip", args);

			llwarns << "Unable to save raid advisor backup data to the notecard." << llendl;
		}
	}
	else
	{
		llwarns << "Unable to find created notecard in inventory, oh god." << llendl;
	}
}
