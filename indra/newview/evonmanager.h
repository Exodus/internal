/** 
 * @file exosystems.h
 * @brief evonManager class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EVON_MANAGER
#define EVON_MANAGER

class evonManager
{
public:
	static S32 toEVON(const LLSD& sd, std::ostream& str, std::string type = "UNDEF", std::string version = "0.0");
	static S32 toPrettyEVON(const LLSD& sd, std::ostream& str, std::string type = "UNDEF", std::string version = "0.0");
	static S32 fromEVON(LLSD& sd, std::istream& str, std::string type = "UNDEF", double version = 0.0);
};

#endif // EVON_MANAGER
